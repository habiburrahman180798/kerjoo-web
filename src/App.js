import { Routes, Route, Outlet, Link } from "react-router-dom";
import Layout from "./app/Layout";
import Home from "./app/Home";
import TestGrid from "./app/TestGrid";
import NotFoundPage from "./app/404";
import TestFlex from "./app/TestFlex";
import TestSelect from "./app/TestSelect";


function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="test-grid" element={<TestGrid />} />
          <Route path="test-flex" element={<TestFlex />} />
          <Route path="test-select" element={<TestSelect />} />

          {/* 404 Not Found Page */}
          <Route path="*" element={<NotFoundPage />} />
        </Route>
      </Routes>
    </div>
  );
}

export default App;
