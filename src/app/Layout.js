import { Outlet, Link } from "react-router-dom";
import { navBarLink } from "../helpers/navigation";

const Layout = () => {
  return (
    <>
      <div>
        <nav>
          <ul>
            {navBarLink.map(({ name, url }) => (
              <li>
                <Link to={url}>{name}</Link>
              </li>
            ))}
          </ul>
        </nav>
        <hr />
        <Outlet />
      </div>
    </>
  )
}

export default Layout;