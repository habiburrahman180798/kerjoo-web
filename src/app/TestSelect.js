import { useDispatch, useSelector } from "react-redux";
import { useGetDistrictsQuery, useGetProvincesQuery, useGetSubDistrictsQuery, useGetVillagesQuery } from "../redux/features/services";
import { handleChangeTestSelect } from "../redux/features/testSelectSlice";
import { store } from "../redux/store";
import { useEffect } from "react";

const TestSelect = () => {
  const state = useSelector((state) => state.testSelect);
  const dispatch = useDispatch();
  const { province, district, subdistrict, village
  } = state

  const { data: provinces } = useGetProvincesQuery();
  const { data: districts } = useGetDistrictsQuery(province, {
    skip: !province,
    refetchOnMountOrArgChange: true,
  });
  const { data: subDistricts } = useGetSubDistrictsQuery(district, {
    skip: !district,
    refetchOnMountOrArgChange: true,
  });
  const { data: villages } = useGetVillagesQuery(subdistrict, {
    skip: !subdistrict,
    refetchOnMountOrArgChange: true,
  });

  const handleInputChange = (fieldName, fieldValue) => {
    console.log(fieldName)
    dispatch(handleChangeTestSelect({ fieldName, fieldValue }));
  };


  console.log(provinces, districts, state, 'sdsdsds')
  return (
    <>
      <div className="d-flex flex-wrap justify-content-around">
        <div>
          <label>Provinsi: </label>
          <select onChange={async (e) => {
            handleInputChange('province', provinces.find((f) => f.name === e.target.value).id)
          }}>
            {provinces && provinces.map((r) => (
              <option>
                {r.name}
              </option>
            ))}
          </select>
        </div>
        <div>
          <label>Kab/Kota: </label>
          <select onChange={async (e) => {
            handleInputChange('district', districts.find((f) => f.name === e.target.value).id)
          }}>
            {districts && districts.map((r) => (
              <option>
                {r.name}
              </option>
            ))}
          </select>
        </div>
        <div>
          <label>Kecamatan: </label>
          <select onChange={async (e) => {
            handleInputChange('subdistrict', subDistricts.find((f) => f.name === e.target.value).id)
          }}>
            {subDistricts && subDistricts.map((r) => (
              <option>
                {r.name}
              </option>
            ))}
          </select>
        </div>
        <div>
          <label>Desa: </label>
          <select onChange={async (e) => {
            handleInputChange('village', villages.find((f) => f.name === e.target.value).id)
          }}>
            {villages && villages.map((r) => (
              <option>
                {r.name}
              </option>
            ))}
          </select>
        </div>
      </div>
    </>
  )
}

export default TestSelect;