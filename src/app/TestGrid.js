import { loremIpsum } from "../helpers/constants";

const TestGrid = () => {
  const grids = [1, 2, 3, 4];
  return (
    <>
      <div className="container mt-4">
        <div className="row">
          {grids.map((grid) => (
            <div key={`grid-${grid}`} className="col-sm-6 col-lg-3">
              <div className="card">
                <div className="card-body">
                  <h5 className="card-title">Item {grid}</h5>
                  <p className="card-text">{loremIpsum}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  )
}

export default TestGrid;