import { configureStore } from '@reduxjs/toolkit';
import { baseApi } from './baseQuery';
import testSelectReducer from './features/testSelectSlice';

export const store = configureStore({
  reducer: {
    [baseApi.reducerPath]: baseApi.reducer,
    testSelect: testSelectReducer
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(baseApi.middleware),
});