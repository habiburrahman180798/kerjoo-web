import { API_URL } from "../../../helpers/config";
import { baseApi } from "../../baseQuery";

const apiWithTag = baseApi.enhanceEndpoints({ addTagTypes: ["test-select"] });

export const testSelectApi = apiWithTag.injectEndpoints({
  overrideExisting: true,
  endpoints: (build) => ({
    getProvinces: build.query({
      query: () => ({
        url: `${API_URL}/provinces`,
        timeout: 1000
      }),
      providesTags: ["test-select"],
    }),
    getDistricts: build.query({
      query: (id) => ({
        url: `${API_URL}/regencies_of/${id}`,
      }),
      providesTags: ["test-select"],
    }),
    getSubDistricts: build.query({
      query: (id) => ({
        url: `${API_URL}/district_of/${id}`,
      }),
      providesTags: ["test-select"],
    }),
    getVillages: build.query({
      query: (id) => ({
        url: `${API_URL}/villages_of/${id}`,
      }),
      providesTags: ["test-select"],
    }),

  }),
});

export const {
  useGetProvincesQuery,
  useGetDistrictsQuery,
  useGetSubDistrictsQuery,
  useGetVillagesQuery,
} = testSelectApi;
