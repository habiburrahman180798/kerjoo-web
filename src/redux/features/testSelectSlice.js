import { createSlice } from '@reduxjs/toolkit';
import { testSelectApi } from './services';

const testSelectSlice = createSlice({
  name: 'testSelect',
  initialState: {
    province: null,
    district: null,
    subdistrict: null,
    village: null
  },
  reducers: {
    handleChangeTestSelect: (state, action) => {
      const { fieldName, fieldValue } = action.payload;
      state[fieldName] = fieldValue;
    },
  },
});

export const { handleChangeTestSelect } = testSelectSlice.actions;

export default testSelectSlice.reducer;