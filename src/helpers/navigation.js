export const navBarLink = [
  {
    name: 'Home',
    url: '/'
  },
  {
    name: 'Test Grid',
    url: '/test-grid'
  },
  {
    name: 'Test Flex',
    url: '/test-flex'
  },
  {
    name: 'Test Select',
    url: '/test-select'
  },
];